﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Kaarten
{
    public class Kaart
    {
        static Assembly _assembly = Assembly.GetExecutingAssembly();
        Stream _imageStream;

        private static List<string> kaartFiguur = new List<string>() { "Clubs", "Diamonds", "Spades", "Hearts" };

        private Bitmap img;
        public Bitmap Img
        { 
        get
            {
                return img;
            }
        }

        private static Bitmap cardBack = (Bitmap)Image.FromStream(_assembly.GetManifestResourceStream(typeof(Kaart), "Resources.back.png"));
        public static Bitmap CardBack
        {
            get
            {
                return cardBack;
            }
        }
        
        private int kaartWaarde;
        public int KaartWaarde
        {
            get { return kaartWaarde; }
            set 
            {
                if (value <= 14 && value >= 1)
                    kaartWaarde = value;
            }
        }

        
        private string figuur;
        public string Figuur
        {
            get { return figuur; }
            set
            {
                if (kaartFiguur.Contains(value))
                    figuur = value;
            }
        }

        public string val;

        public Kaart(int val, string s)
        {
            this.KaartWaarde = val;
            this.Figuur = s;
            this._imageStream = imgS();
            this.img = (Bitmap)Image.FromStream(_imageStream);
        }

        private Stream imgS()
        {
            this.val = this.kaartWaarde.ToString();
            string suit = this.figuur.Substring(0, 1);
            switch (this.KaartWaarde)
            {
                case 1:
                    this.val = "A";
                    break;
                case 11:
                    this.val = "J";
                    break;
                case 12:
                    this.val = "Q";
                    break;
                case 13:
                    this.val = "K";
                    break;
                default:
                    this.val = this.KaartWaarde.ToString();
                    break;
            }
            try
            {
                
                Stream st;
                if (val.All(Char.IsDigit))
                {
                    string name = string.Format("Resources._{0}{1}.png", val, suit);
                    st = _assembly.GetManifestResourceStream(typeof(Kaart), name);
                }
                else
                {
                    string name = string.Format("Resources.{0}{1}.png", val, suit);
                    st = _assembly.GetManifestResourceStream(typeof(Kaart), name);
                }
                   
                if (st != null)
                    return st;
                else
                    return null ;
            }
            catch
            {
                throw new Exception("Kaart kan niet geladen worden.");
            }
        }
    }
}