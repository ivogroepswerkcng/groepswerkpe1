﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Kaarten
{
    public class Hand : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool isSelected;

        public bool IsSelected
        {
            get { return isSelected; }
            set 
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        private string handNaam;
        public string HandNaam
        {
            get { return handNaam; }
            set { handNaam = value; }
        }

        private int betaling;

        public int Betaling
        {
            get { return betaling; }
        }

        public static List<string> hands = new List<string>() { "Royal Flush", "Straight Flush", "Four of a Kind", "Full House", "Flush", "Straight", "Three of a Kind", "Two Pair", "Jacks or better" };

        /// <summary>
        /// Nieuwe mogelijke hand aanmaken
        /// </summary>
        /// <param name="s">Engelse naam van de hand</param>
        /// <param name="inzet">Het aantal credits die ingezet worden</param>
        /// <exception cref="Exception">De meegegeven handnaam bestaat niet in de static List(van string)hands</exception>
        public Hand(string s, int inzet)
        {
            if (hands.Contains(s))
            {
                //Waarde toevoegen aan betaling
                handNaam = s;
                switch (handNaam)
                {
                    case "Royal Flush":
                        if (inzet != 50)
                            betaling = inzet * 250;
                        else betaling = 40000;
                        break;
                    case "Straight Flush":
                        betaling = inzet * 50;
                        break;
                    case "Four of a Kind":
                        betaling = inzet * 25;
                        break;
                    case "Full House":
                        betaling = inzet * 9;
                        break;
                    case "Flush":
                        betaling = inzet * 6;
                        break;
                    case "Straight":
                        betaling = inzet * 4;
                        break;
                    case "Three of a Kind":
                        betaling = inzet * 3;
                        break;
                    case "Two Pair":
                        betaling = inzet * 2;
                        break;
                    case "Jacks or better":
                        betaling = inzet;
                        break;
                }
            }
            else
            {
                throw new Exception("Deze hand is niet mogelijk \nGelieve een waarde te halen uit Hand.hands");
            }
        }

        public static bool IsStraat(List<Kaart> kList)
        {
            var count = kList.Count;
            var straights = kList.Select(k => k.KaartWaarde)
                .Distinct()
                .OrderBy(k => k)
                .Select(k => k + (count--))
                .GroupBy(n => n)
                .OrderByDescending(group => group.Count());

            var longestStraight = straights.First().Count();
            if (longestStraight == 5)
                return true;
            else if (longestStraight == 4 && (kList.Select(k => k.KaartWaarde).OrderBy(k => k).First() == 1 || kList.Select(k => k.KaartWaarde).OrderBy(k => k).Last() == 1))
            {
                kList[kList.IndexOf(kList.Find(k => k.KaartWaarde == 1))].KaartWaarde = 14;
                count = kList.Count;
                straights = kList.Select(k => k.KaartWaarde)
                    .Distinct()
                    .OrderBy(k => k)
                    .Select(k => k + (count--))
                    .GroupBy(n => n)
                    .OrderByDescending(group => group.Count());

                longestStraight = straights.First().Count();
                kList[kList.IndexOf(kList.Find(k => k.KaartWaarde == 14))].KaartWaarde = 1;
                if (longestStraight == 5)
                    return true;
                else
                    return false;
            }
            else 
                return false;
        }

        public static bool IsFlush(List<Kaart> kList)
        {
            var first = kList.Select(k => k.Figuur).OrderBy(k => k).First();
            var last = kList.Select(k => k.Figuur).OrderBy(k => k).Last();
            if (first == last)
                return true;
            else return false;
        }

        public static bool IsFourKind(List<Kaart> kList)
        {
            if (kList.GroupBy(k => k.KaartWaarde).Any(l => l.Count() == 4))
                return true;
            else
                return false;
        }

        public static bool IsThreeKind(List<Kaart> kList)
        {
            if (kList.GroupBy(k => k.KaartWaarde).Any(l => l.Count() >= 3))
                return true;
            else
                return false;
        }

        public static bool IsTwoPair(List<Kaart> kList)
        {
            if (kList.GroupBy(k => k.KaartWaarde).Count(g => g.Count() >= 2) == 2)
            {
                return true;
            }
            else return false;
        }

        public static int FindPair(List<Kaart> kList)
        {
            IEnumerable<IGrouping<int, Kaart>> groups = kList.GroupBy(k => k.KaartWaarde).Where(l => l.Count() == 2);
            IEnumerable<Kaart> cards = groups.SelectMany(g => g);
            List<Kaart> newList = cards.ToList();
            if (newList.Count != 0)
                return newList[0].KaartWaarde;
            else return 0;
        }

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
