﻿using System;
using System.Collections.Generic;

namespace Kaarten
{
    public class Kaartspel
    {
        public List<Kaart> kaarten = new List<Kaart>();
        int z = 4;


        /// <summary>
        /// Maak een nieuw aantal kaartspellen (52 kaarten)
        /// </summary>
        public Kaartspel()
        {
            string[] suits = new string[4] { "Spades", "Hearts", "Clubs", "Diamonds" };
                for (int i = 1; i <= 4; i++)
                {
                    string s = suits[i - 1];
                    for (int o = 1; o <= 13; o++)
                    {
                        try
                        {
                            this.kaarten.Add(new Kaart(o, s));
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
        }

        public void Shuffle()
        {
            this.kaarten.Shuffle();
            this.z = 4;
        }

        public Kaart NieuweKaart()
        {
            return kaarten[z++];
        }
    }

    static class MyExtensions
    {
        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}
