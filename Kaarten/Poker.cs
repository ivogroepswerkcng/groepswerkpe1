﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kaarten
{
    public static class Poker
    {
        public static int VindWinst(List<Kaart> splK, List<Hand> hands, out Hand handNaam)
        {
            splK = splK.OrderBy(x => x.KaartWaarde).ToList();
            string hand = CheckScores(splK);
            handNaam = hands.Find(h => h.HandNaam == hand);
            try
            {
                return hands.Find(h => h.HandNaam == hand).Betaling;
            }
            catch (NullReferenceException)
            {
                return 0;
            }
        }

        /// <summary>
        /// Zoek mogelijke scores met de huidige kaarten
        /// </summary>
        /// <param name="ks">Het kaartspel die gebruikt word</param>
        /// <returns></returns>
        public static string CheckScores(List<Kaart> ks)
        {

            bool straat = Hand.IsStraat(ks);
            bool flush = Hand.IsFlush(ks);
            bool tok = Hand.IsThreeKind(ks);
            bool hasPair = false;
            int pair = Hand.FindPair(ks);
            if (pair != 0)
                hasPair = true;

            if (flush && straat && (ks[ks.Count - 1].KaartWaarde == 1))
                return "Royal Flush";
            else if (flush && straat)
                return "Straight Flush";
            else if (Hand.IsFourKind(ks))
                return "Four of a Kind";
            else if (tok && hasPair)
                return "Full House";
            else if (flush)
                return "Flush";
            else if (straat)
                return "Straight";
            else if (tok)
                return "Three of a Kind";
            else if (Hand.IsTwoPair(ks))
                return "Two Pair";
            else if (pair == 1 || pair >= 11)
                return "Jacks or better";
            else
                return "Niets";
        }
    }
}
