﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;

namespace Account
{
    public class Login
    {
        //Declareer een SqlConnection variabele die zal moeten ingevuld worden vanuit de eigenlijke applicatie.
        //Dit omdat er in een Class Library geen app.config bestand is.
        public static SqlConnection con = null;

        //Standaard waarden voor de "anonieme" account.
        //Deze waarden worden gebruikt voor gebruikers die niet inloggen.
        public int uID = 0;
        private int cred = 2500;
        public int Cred
        {
            get { return cred; }
            set 
            {
                //De waarde kan niet onder nul
                if (value >= 0)
                    cred = value;
                else
                    return;
            }
        }

        public string loginNaam = "";
        
        /// <summary>
        /// Gebruiker logt niet in
        /// Standaardwaarden worden gebruikt.
        /// </summary>
        public Login() { }

        /// <summary>
        /// De gebruiker logt in met een e-mail adres of gebruikersnaam.
        /// De waarden worden gehaald uit de database.
        /// </summary>
        /// <param name="uName">Gebruikersnaam of E-mail adres</param>
        /// <param name="pwd">Wachtwoord</param>
        public Login(string uName, string pwd)
        {
            string cmdText = "";
            string pwdHash = "";
            int id = 0;

            if (Mail(uName)) //Kijk of het eerste veld ingevuld werd met een e-mail adres of met een gebruikersnaam
                cmdText = string.Format("SELECT AccID, AccPwd, AccCredits, AccNaam FROM Logins WHERE AccEmail = '{0}'", uName);
            else
                cmdText = string.Format("SELECT AccID, AccPwd, AccCredits, AccNaam FROM Logins WHERE AccNaam = '{0}'", uName);

            SqlCommand cmd = new SqlCommand(cmdText, con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            SqlDataReader rdr = null;

            try
            {
                con.Open();
                rdr = cmd.ExecuteReader();
                if (!rdr.HasRows)
                    throw new Exception("Gebruiker niet gevonden.");
                else
                {
                    rdr.Read();
                    id = rdr.GetInt32(0);
                    pwdHash = rdr.GetString(1);
                    cred = rdr.GetInt32(2);
                    loginNaam = rdr.GetString(3).Substring(0, 1).ToUpper() + rdr.GetString(3).Substring(1, rdr.GetString(3).Length - 1);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                if (rdr != null)
                    rdr.Close();
                if (con != null)
                    con.Close();
            }
            if (PasswordHash.ValidatePassword(pwd, pwdHash))
                uID = id;
            else
                throw new Exception("Verkeerd wachtwoord.");
        }

        /// <summary>
        /// Registreer een nieuwe account
        /// </summary>
        /// <param name="uname">Gebruikersnaam</param>
        /// <param name="pwd">Wachtwoord</param>
        /// <param name="email">E-mail adres</param>
        /// <returns>String met boodschap voor de gebruiker.</returns>
        public static string Registreer(string uname, string pwd, string email)
        {
            int id = 0;
            try
            {
                if (!Mail(email))
                    throw new FormatException();
                using (SqlCommand cmd = new SqlCommand("Register", con) { CommandType = CommandType.StoredProcedure })
                {
                    cmd.Parameters.Add("@uname", SqlDbType.NVarChar, 20).Value = uname;
                    cmd.Parameters.Add("@pwd", SqlDbType.NVarChar, 70).Value = pwd;
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar, 254).Value = email;
                    try
                    {
                        con.Open();
                        id = Convert.ToInt32(cmd.ExecuteScalar());
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Writer: " + ex.Message);
                    }
                    finally
                    {
                        if (con != null)
                            con.Close();
                    }
                }
            }
            catch (FormatException)
            {
                id = 0;
            }

            string message = "";

            switch (id)
            {
                case -1:
                    message = "Deze gebruikersnaam bestaat al.\nKies een andere.";
                    break;
                case -2:
                    message = "Dit e-mail adres bestaat al, probeer opnieuw.";
                    break;
                case 0:
                    message = "Het e-mail adres is niet van het juiste formaat.";
                    break;
                default:
                    message = "U bent geregistreerd, U bent aangemeld.";
                    break;
            }
            return message;
        }

        /// <summary>
        /// Afmelden
        /// Om te testen of het afmelden gewerkt heeft, kun je kijken of de uID op 0 staat.
        /// </summary>
        public void Logout()
        {
            uID = 0;
            cred = 2500;
        }

        /// <summary>
        /// Een check om te zien of de string die je doorzend een geldig mail adres is.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>True = geldig, False = ongeldig</returns>
        private static bool Mail(string email)
        {
            try
            {
                MailAddress ma = new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Update de score in de database.
        /// Best om dit te doen na elke keer een nieuw spel start of eindigt.
        /// </summary>
        /// <exception cref="Exception">Word opgeroepen als er geen connectie mogelijk is met de database.</exception>
        public  void UpdateScore()
        {
            if (uID != 0)
            {
                string cmdText = string.Format("UPDATE Logins SET AccCredits = {0} WHERE AccID = {1}", this.cred, uID);
                using (SqlCommand cmd = new SqlCommand(cmdText, con))
                {
                    try
                    {
                        con.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        if (con != null)
                            con.Close();
                    }
                }
            }
        }

        public static void DeleteAccount(int id)
        {
            string cmdText = string.Format("DELETE FROM Logins WHERE AccID = {0}", id);
            using (SqlCommand cmd = new SqlCommand(cmdText, con))
            {
                try
                {
                    con.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
                finally
                {
                    if (con != null)
                        con.Close();
                }
            }
        }
    }
}
