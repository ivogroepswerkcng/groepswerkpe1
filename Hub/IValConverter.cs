﻿using System;
using System.Windows.Data;
using System.Windows.Media;

namespace Hub
{
    [ValueConversion(typeof(Color), typeof(SolidColorBrush))]
    public class ColorBrushConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool val = false;
            try
            {
                val = (bool)value;
            }
            catch (Exception)
            { }
            if (val)
            {
                return new SolidColorBrush(Color.FromArgb(255,50,50,50));
            }
            else
                return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }
    }
}
