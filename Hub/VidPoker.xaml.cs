﻿using Kaarten;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Hub
{
    /// <summary>
    /// Interaction logic for VidPoker.xaml
    /// </summary>
    public partial class VidPoker : Window
    {
        //Hoofdscherm declareren.
        MainWindow mw = MainWindow.instance;

        //Lijsten van controls
        List<Image> imgs = new List<Image>();
        List<CheckBox> chks = new List<CheckBox>();

        //Kaartspel aanmaken
        Kaartspel ks = new Kaartspel();

        //Lijsten aanmaken voor functionaliteit
        List<Hand> hands = new List<Hand>();
        List<Kaart> splK = new List<Kaart>();
        List<Kaart> gebruikteKaarten = new List<Kaart>();
        
        //Rest van nodige variabelen
        Random r = new Random();
        int inzet = 10;
        int deals = 0;

        public VidPoker()
        {
            InitializeComponent();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            mw.ShowMain();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (Image i in FindVisualChildren<Image>(this))
            {
                imgs.Add(i);
            }

            imgKaart1.Source = Imaging.CreateBitmapSourceFromHBitmap(ks.kaarten[22].Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            imgKaart2.Source = Imaging.CreateBitmapSourceFromHBitmap(ks.kaarten[23].Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            imgKaart3.Source = Imaging.CreateBitmapSourceFromHBitmap(ks.kaarten[24].Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            imgKaart4.Source = Imaging.CreateBitmapSourceFromHBitmap(ks.kaarten[25].Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            imgKaart5.Source = Imaging.CreateBitmapSourceFromHBitmap(ks.kaarten[13].Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
            ks.Shuffle();

            foreach (CheckBox c in FindVisualChildren<CheckBox>(this))
            {
                chks.Add(c);
            }
            lblCreds.Content = MainWindow.sessie.Cred.ToString();
            InzetVeranderd();
        }

        private void btnInzetVerhogen_Click(object sender, RoutedEventArgs e)
        {
            if (inzet != 50)
                inzet = inzet + 10;
            else
                inzet = 10;
                
            InzetVeranderd();

            lblInzet.Content = inzet.ToString();
        }

        private void btnMax_Click(object sender, RoutedEventArgs e)
        {
                inzet = 50;

                InzetVeranderd();

                lblInzet.Content = inzet.ToString();

                if (deals == 0)
                {
                    StartSpel();
                }
        }

        void InzetVeranderd()
        {
            hands.Clear();
            lvTest.ItemsSource = null;
            for (int i = 0; i <= 8; i++)
            {
                hands.Add(new Hand(Hand.hands[i], inzet));
            }
            lvTest.ItemsSource = hands;
        }

        private void btnDeal_Click(object sender, RoutedEventArgs e)
        {
            if (deals == 0)
            {
                StartSpel();
            }
            else
            {
                deals = 0;
                if (splK.Count != 0)
                    EindeSpel();
            }
        }

        private void StartSpel()
        {
            gebruikteKaarten.Clear();
            splK.Clear();
            if (MainWindow.sessie.Cred < inzet)
                MessageBox.Show("U heeft te weinig credits over.", "Blut!");
            else
            {
                lblWinst.Content = "0";
                MainWindow.sessie.Cred -= inzet;
                MainWindow.sessie.UpdateScore();
                lblCreds.Content = MainWindow.sessie.Cred.ToString();

                foreach (CheckBox c in chks)
                    c.IsEnabled = true;

                //Deel 5 willekeurige kaarten uit
                while (splK.Count != 5)
                {
                    VindKaart();
                }
                deals = 1;
                btnInzetVerhogen.IsEnabled = false;
                btnMax.IsEnabled = false;
                foreach (Hand h in lvTest.Items)
                    h.IsSelected = false;
            }
        }

        private void VindKaart()
        {
            Kaart k = ks.kaarten[r.Next(0, 52)];
            if (!gebruikteKaarten.Contains(k))
            {
                splK.Add(k);
                imgs[splK.IndexOf(k)].Source = Imaging.CreateBitmapSourceFromHBitmap(k.Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                gebruikteKaarten.Add(k);
            }
        }

        private void NieuweKaart(int index)
        {
            Kaart k = ks.kaarten[r.Next(0, 52)];
            if (!gebruikteKaarten.Contains(k))
            {
                splK[index] = k;
                imgs[index].Source = Imaging.CreateBitmapSourceFromHBitmap(k.Img.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
                gebruikteKaarten.Add(k);
            }
        }

        private void EindeSpel()
        {
            foreach (CheckBox c in chks)
            {
                if (!(bool)c.IsChecked)
                {
                    NieuweKaart(chks.IndexOf(c));
                }
                c.IsChecked = false;
                c.IsEnabled = false;
            }
            btnInzetVerhogen.IsEnabled = true;
            btnMax.IsEnabled = true;
            int winst;
            Hand hand = null;
            winst = Poker.VindWinst(splK, hands.ToList(), out hand);
            if (hand != null)
            {
                foreach (Hand h in lvTest.Items)
                {
                    if (h.HandNaam == hand.HandNaam)
                    {
                        h.IsSelected = true;
                    }
                }
            }
            lblWinst.Content = winst.ToString();
            MainWindow.sessie.Cred += winst;
            lblCreds.Content = MainWindow.sessie.Cred.ToString();
            try
            {
                MainWindow.sessie.UpdateScore();
            }
            catch (Exception)
            {
                MessageBox.Show("Er is geen verbinding met de database.");
            }
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}