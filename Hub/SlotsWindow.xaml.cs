﻿using Slotslib;
using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Hub
{
    /// <summary>
    /// Interaction logic for SlotsWindow.xaml
    /// </summary>
    public partial class SlotsWindow : Window
    {
        MainWindow mw = MainWindow.instance;

        Slotmachine s = new Slotmachine();

        DispatcherTimer timer1 = new DispatcherTimer();

        private Slot spinSlot1 = new Slot();
        private Slot spinSlot2 = new Slot();
        private Slot spinSlot3 = new Slot();

        private string geenCredits = "U heeft geen credits meer.";
        public SlotsWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            s.Credits = MainWindow.sessie.Cred;
            lblCredits.Content = s.Credits;
            lblInzet.Content = s.Inzet;
            lblGewonnenBedrag.Content = s.GewonnenBedrag;
            lblBericht.Content = s.Bericht;
            btnCollect.IsEnabled = false;
            btnSpin.IsEnabled = false;
            btnStop.IsEnabled = false;
            imgSlot1.Source = new BitmapImage(new Uri("images/Zeven.jpg", UriKind.Relative));
            imgSlot2.Source = new BitmapImage(new Uri("images/Zeven.jpg", UriKind.Relative));
            imgSlot3.Source = new BitmapImage(new Uri("images/Zeven.jpg", UriKind.Relative));

        }
        private void btnSpin_Click(object sender, RoutedEventArgs e)
        {
            timer1.Tick += timer1_Tick;
            timer1.Interval = new TimeSpan(10000 * 100);
            timer1.Start();
            btnStop.IsEnabled = true;
            btnSpin.IsEnabled = false;
            btnVerhoogMet5.IsEnabled = false;
            btnVerhoogMet10.IsEnabled = false;
            btnVerhoogMet20.IsEnabled = false;
            btnVerhoogMet50.IsEnabled = false;
        }
        void timer1_Tick(object sender, EventArgs e)
        {
            spinSlot1 = new Slot();
            spinSlot2 = new Slot();
            spinSlot3 = new Slot();
            imgSlot1.Source = new BitmapImage(new Uri("images/" + spinSlot1.Afbeelding + ".jpg", UriKind.Relative));
            imgSlot2.Source = new BitmapImage(new Uri("images/" + spinSlot2.Afbeelding + ".jpg", UriKind.Relative));
            imgSlot3.Source = new BitmapImage(new Uri("images/" + spinSlot3.Afbeelding + ".jpg", UriKind.Relative));
        }
        private void btnVerhoogMet5_Click(object sender, RoutedEventArgs e)
        {
            s.VerhoogMet5();
            lblInzet.Content = s.Inzet;
            lblCredits.Content = s.Credits;
            btnSpin.IsEnabled = true;
            lblBericht.Content = s.Bericht;
        }
        private void btnVerhoogMet10_Click(object sender, RoutedEventArgs e)
        {
            s.VerhoogMet10();
            lblInzet.Content = s.Inzet;
            lblCredits.Content = s.Credits;
            btnSpin.IsEnabled = true;
            lblBericht.Content = s.Bericht;
        }
        private void btnVerhoogMet20_Click(object sender, RoutedEventArgs e)
        {
                s.VerhoogMet20();
                lblInzet.Content = s.Inzet;
                lblCredits.Content = s.Credits;
                btnSpin.IsEnabled = true;
                lblBericht.Content = s.Bericht;
        }

        private void btnVerhoogMet50_Click(object sender, RoutedEventArgs e)
        {
                s.VerhoogMet50();
                lblInzet.Content = s.Inzet;
                lblCredits.Content = s.Credits;
                btnSpin.IsEnabled = true;
                lblBericht.Content = s.Bericht;
        }


        private void btnCollect_Click(object sender, RoutedEventArgs e)
        {
            s.Collect();
            MainWindow.sessie.Cred = s.Credits;
            lblCredits.Content = MainWindow.sessie.Cred;
            try
            {
                MainWindow.sessie.UpdateScore();
            }
            catch (Exception)
            {
                MessageBox.Show("Er is geen verbinding met de database.");
            }
            lblInzet.Content = s.Inzet;
            lblGewonnenBedrag.Content = s.GewonnenBedrag;
            lblBericht.Content = s.Bericht;
            btnVerhoogMet5.IsEnabled = true;
            btnVerhoogMet10.IsEnabled = true;
            btnVerhoogMet20.IsEnabled = true;
            btnVerhoogMet50.IsEnabled = true;
            btnCollect.IsEnabled = false;
            if (s.Credits == 0)
            {
                MessageBox.Show(geenCredits);
                btnVerhoogMet5.IsEnabled = false;
                btnVerhoogMet10.IsEnabled = false;
                btnVerhoogMet20.IsEnabled = false;
                btnVerhoogMet50.IsEnabled = false;
            }
        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            timer1.Stop();
            s.MaakNieuweCombinatie();
            imgSlot1.Source = new BitmapImage(new Uri("images/" + s.slot1.Afbeelding + ".jpg", UriKind.Relative));
            imgSlot2.Source = new BitmapImage(new Uri("images/" + s.slot2.Afbeelding + ".jpg", UriKind.Relative));
            imgSlot3.Source = new BitmapImage(new Uri("images/" + s.slot3.Afbeelding + ".jpg", UriKind.Relative));
            lblBericht.Content = s.Bericht;
            lblCredits.Content = s.Credits;
            lblInzet.Content = s.Inzet;
            lblGewonnenBedrag.Content = s.GewonnenBedrag;
            btnSpin.IsEnabled = false;
            btnVerhoogMet5.IsEnabled = false;
            btnVerhoogMet10.IsEnabled = false;
            btnVerhoogMet20.IsEnabled = false;
            btnVerhoogMet50.IsEnabled = false;
            btnCollect.IsEnabled = true;
            btnStop.IsEnabled = false;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            mw.ShowMain();
        }
    }
}
