﻿using Account;
using System;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Windows;

namespace Hub
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static Login sessie = new Login();
        public static MainWindow instance;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginSystem ls = new LoginSystem();
            ls.Show();
            this.Visibility = Visibility.Collapsed;
        }

        private void btnLogout_Click(object sender, RoutedEventArgs e)
        {
            sessie.Logout();
            btnLogout.Visibility = Visibility.Collapsed;
            btnDeleteAcc.Visibility = Visibility.Collapsed;
            btnLogin.Visibility = Visibility.Visible;
            if (sessie.uID == 0)
            {
                MessageBox.Show("Succesvol afgemeld.");
                lblAangemeld.Content = "Niet aangemeld";
                lblCredits.Content = "Credits: " + sessie.Cred;
            }
        }

        private void btnVidPoker_Click(object sender, RoutedEventArgs e)
        {
            VidPoker bj = new VidPoker();
            bj.Show();
            this.Visibility = Visibility.Collapsed;
        }

        private void btnSlots_Click(object sender, RoutedEventArgs e)
        {
            SlotsWindow sw = new SlotsWindow();
            sw.Show();
            this.Visibility = Visibility.Collapsed;
        }

        public void ShowMain()
        {
            this.Visibility = Visibility.Visible;
            lblCredits.Content = "Credits: " + sessie.Cred;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            instance = this;
            string loc = Assembly.GetExecutingAssembly().Location;

            if (File.Exists(loc + ".config"))
                File.Delete(loc + ".config");
            string result = "";
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Hub.Resources.Hub.exe.config"))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = reader.ReadToEnd();
            }

            using (StreamWriter wr = new StreamWriter(loc + ".config"))
            {
                wr.Write(result);
            }

            //Config bestand encrypteren
             Configuration conf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection s = conf.GetSection("connectionStrings");
            if (s != null)
            {
                s.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                s.SectionInformation.ForceSave = true;
                conf.Save(ConfigurationSaveMode.Full);
            }
        }

        private void btnResetCred_Click(object sender, RoutedEventArgs e)
        {
            if (sessie.Cred < 2500)
            {
                sessie.Cred = 2500;
                lblCredits.Content = "Credits: " + sessie.Cred;
                sessie.UpdateScore();
            }
        }

        private void btnDeleteAcc_Click(object sender, RoutedEventArgs e)
        {
            if (sessie.uID != 0)
            {
                try
                {
                    Login.DeleteAccount(sessie.uID);
                    MessageBox.Show("Account verwijderd");
                    sessie.Logout();
                    btnLogout.Visibility = Visibility.Collapsed;
                    btnDeleteAcc.Visibility = Visibility.Collapsed;
                    btnLogin.Visibility = Visibility.Visible;
                    if (sessie.uID == 0)
                    {
                        MessageBox.Show("Succesvol afgemeld.");
                        lblAangemeld.Content = "Niet aangemeld";
                        lblCredits.Content = "Credits: " + sessie.Cred;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                MessageBox.Show("Je moet eerst aanmelden om je account te kunnen verwijderen!");
        }
    }
}