﻿using Account;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Hub
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginSystem : Window
    {
        List<string> std = new List<string>();
        List<TextBox> lt = new List<TextBox>();
        MainWindow mw = MainWindow.instance;

        public LoginSystem()
        {
            InitializeComponent();
        }

        private void btnMaak_Click(object sender, RoutedEventArgs e)
        {
            string uname = txtUName.Text;
            string pwd = txtPwd.Password;
            Regex rUsername = new Regex("^[a-zA-Z0-9]*$");

            //Kijk of de gebruikersnaam en wachtwoord de regels volgen
            //Regels: 
            //minimum 6 tekens (a-z, 0-9) voor de gebruikersnaam
            //Minimum 8 tekens (A-Z, a-z, 0-9, speciale tekens, behalve ') voor wachtwoord
            if ((uname.Length < 6) || (uname.Length > 10) || !rUsername.IsMatch(uname))
            {
                MessageBox.Show("Uw gebruikersnaam moet een lengte hebben van 6 tot en met 10 tekens\nen mag enkel alfanumerieke tekens bevatten.");
                txtUName.Foreground = Brushes.Red;
            }
            else if (pwd.Length < 8 || pwd.Length > 20 || pwd.Contains("'"))
            {
                MessageBox.Show("Uw wachtwoord moet een lengte hebben van 8 tot en met 20 tekens\nen mag alles bevatten behalve \"'\".");
                txtPwd.Foreground = Brushes.Red;
            }
            else
            {
                string pwdHash = PasswordHash.CreateHash(pwd);
                string email = txtEmail.Text;
                string message = "";

                try
                {
                    message = Login.Registreer(uname, pwdHash, email);
                    txtUName.Text = "";
                    txtPwd.Password = "";
                    txtEmail.Text = "";
                    txtUName.Focus();
                    txtPwd.Focus();
                    txtEmail.Focus();
                    txtLogin.Focus();
                    MainWindow.sessie = new Login(uname, pwd);
                    mw.lblAangemeld.Content = "Aangemeld als " + MainWindow.sessie.loginNaam;
                    mw.lblCredits.Content = "Credits: " + MainWindow.sessie.Cred;
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                MessageBox.Show(message);
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            string uname = txtLogin.Text;
            string pwd = txtPwdLogin.Password;

            try
            {
                MainWindow.sessie = new Login(uname, pwd);
                mw.lblAangemeld.Content = "Aangemeld als " + MainWindow.sessie.loginNaam;
                mw.lblCredits.Content = "Credits: " + MainWindow.sessie.Cred;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            mw.ShowMain();
            if (MainWindow.sessie.uID > 0)
            {
                mw.btnLogin.Visibility = Visibility.Hidden; 
                mw.btnLogout.Visibility = Visibility.Visible;
                mw.btnDeleteAcc.Visibility = Visibility.Visible;
            }
            else
            {
                mw.btnLogin.Visibility = Visibility.Visible; 
                mw.btnLogout.Visibility = Visibility.Hidden;
                mw.btnDeleteAcc.Visibility = Visibility.Hidden;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //App.config Decrypteren & her-encrypteren.
            Configuration conf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            ConfigurationSection s = conf.GetSection("connectionStrings");
            if (s != null && s.SectionInformation.IsProtected)
            {
                s.SectionInformation.UnprotectSection();
                Login.con = new SqlConnection(ConfigurationManager.ConnectionStrings["thuisDB"].ConnectionString);
                s.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider");
                s.SectionInformation.ForceSave = true;
                conf.Save(ConfigurationSaveMode.Full);
            }
            std.Add("Gebruikersnaam");
            std.Add("Wachtwoord");
            std.Add("E-mail adres");
            std.Add("Gebruikersnaam/E-mail adres");
            foreach (TextBox t in FindVisualChildren<TextBox>(this))
            {
                lt.Add(t);
                t.GotFocus += (blender, f) => ClearWaterMark(sender, e, t);
                t.LostFocus += (blender, f) => WaterMarks(sender, e, t);
                WaterMarks(null, null, t);
            }
        }

        void ClearWaterMark(object sender, EventArgs e, TextBox t)
        {
            if (std.Contains(t.Text))
                t.Text = "";
            t.Foreground = Brushes.Black;
        }

        void WaterMarks(object sender, EventArgs e, TextBox t)
        {
            if (t.Text == "")
            {
                switch (t.Name)
                {
                    case "txtUName":
                        t.Text = "Gebruikersnaam";
                        break;
                    case "txtPwd":
                        t.Text = "Wachtwoord";
                        break;
                    case "txtEmail":
                        t.Text = "E-mail adres";
                        break;
                    case "txtLogin":
                        t.Text = "Gebruikersnaam/E-mail adres";
                        break;
                    case "txtPwdLogin":
                        t.Text = "Wachtwoord";
                        break;
                }
                t.Foreground = Brushes.Gray;
            }
                
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }
    }
}