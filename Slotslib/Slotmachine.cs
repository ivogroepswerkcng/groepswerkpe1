﻿using System;

namespace Slotslib
{
    public class Slot
    {
        public string Afbeelding { get; set; }

        private static Random rnd = new Random();

        public string[] mogelijkheden = { "Appel", "Citroen", "Druif", "Aardbei", "Kers", "Bel", "Zeven" };


        public Slot()
        {
            MaakNieuwSlot();
        }

        public void MaakNieuwSlot()
        {
            int aantalMogelijkheden = mogelijkheden.Length;
            Afbeelding = mogelijkheden[rnd.Next(0, aantalMogelijkheden)];
        }
    
    }
    

    public class Slotmachine
    {

        public Slot slot1;
        public Slot slot2;
        public Slot slot3;
        public string Bericht { get; set; }
        public int Credits { get; set; }
        public int Inzet { get; set; }

        public int GewonnenBedrag { get; set; }


        public Slotmachine()
        {
            Credits = 1000;
            Inzet = 0;
            GewonnenBedrag = 0;
            Bericht = "Please place your bet";
            slot1 = new Slot();
            slot2 = new Slot();
            slot3 = new Slot();
        }

        public void MaakNieuweCombinatie()
        {
            slot1 = new Slot();
            slot2 = new Slot();
            slot3 = new Slot();
            if (slot1.Afbeelding == slot2.Afbeelding && slot2.Afbeelding == slot3.Afbeelding)
            {
                if (slot1.Afbeelding == slot1.mogelijkheden[0] || slot1.Afbeelding == slot1.mogelijkheden[1])
                {
                    Bericht = "Bet X 3";
                    GewonnenBedrag = Inzet * 3;
                }
                else if (slot1.Afbeelding == slot1.mogelijkheden[2] || slot1.Afbeelding == slot1.mogelijkheden[3])
                {
                    Bericht = "Bet X 4";
                    GewonnenBedrag = Inzet * 4;
                }
                else if (slot1.Afbeelding == slot1.mogelijkheden[4] || slot1.Afbeelding == slot1.mogelijkheden[5])
                {
                    Bericht = "Bet X 5";
                    GewonnenBedrag = Inzet * 5;
                }
                else if (slot1.Afbeelding == slot1.mogelijkheden[6])
                {
                    Bericht = "JACKPOT! Bet x 10";
                    GewonnenBedrag = Inzet * 10;
                }
            }
            else if (slot1.Afbeelding == slot2.Afbeelding || slot2.Afbeelding == slot3.Afbeelding)
            {
                if (slot2.Afbeelding == slot1.mogelijkheden[0] || slot2.Afbeelding == slot1.mogelijkheden[1])
                {
                    Bericht = "Bet X 1";
                    GewonnenBedrag = Inzet;
                }
                else if (slot2.Afbeelding == slot1.mogelijkheden[2] || slot2.Afbeelding == slot1.mogelijkheden[3])
                {
                    Bericht = "Inzet X 2";
                    GewonnenBedrag = Inzet * 2;
                }
                else if (slot2.Afbeelding == slot1.mogelijkheden[4] || slot2.Afbeelding == slot1.mogelijkheden[5])
                {
                    Bericht = "Inzet X 3";
                    GewonnenBedrag = Inzet * 3;
                }
                else if (slot2.Afbeelding == slot1.mogelijkheden[6])
                {
                    Bericht = "Inzet X 4";
                    GewonnenBedrag = Inzet * 4;
                }
            }
            else
            {
                Bericht = "Lost";
                GewonnenBedrag = 0;
            }
        }

        public void VerhoogMet5()
        {
            if (Credits >= 5)
            {
                int plus5 = 5;
                Credits = Credits - plus5;
                Inzet = Inzet + plus5;
                Bericht = "Please place your bet";
            }
            else
            {
                Bericht = "Not enough credits";
            }
        }

        public void VerhoogMet10()
        {
            if (Credits >= 10)
            {
                int plus10 = 10;
                Credits = Credits - plus10;
                Inzet = Inzet + plus10;
                Bericht = "Please place your bet";
            }
            else
            {
                Bericht = "Not enough credits";
            }
        }
        public void VerhoogMet20()
        {
            if (Credits >= 20)
            {
                int plus20 = 20;
                Credits = Credits - plus20;
                Inzet = Inzet + plus20;
                Bericht = "Please place your bet";
            }
            else
            {
                Bericht = "Not enough credits";
            }
        }
        public void VerhoogMet50()
        {
            if (Credits >= 50)
            {
                int plus50 = 50;
                Credits = Credits - plus50;
                Inzet = Inzet + plus50;
                Bericht = "Please place your bet";
            }
            else
            {
                Bericht = "Not enough credits";
            }
        }
        public void Collect()
        {
            Credits = Credits + GewonnenBedrag;
            Inzet = 0;
            GewonnenBedrag = 0;
            Bericht = "Please place your bet";
        }
    }

}
